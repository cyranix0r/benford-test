#!/usr/bin/env ruby

BEGIN {
   if ARGV.length != 1
      puts "Usage: #{PROGRAM_NAME} <file to test>"
      exit
   end

   file = File.new("#{ARGV[0]}", "rb")
   if !file
      puts "Unable to open #{ARGV[0]}"
      exit
   end
}

count = Array.new()

file.each_byte { |byte|
   byte.to_s.split('').each { |char|
      count[char.to_i] ? count[char.to_i] += 1 : count[char.to_i] = 1
   }
}

END {
   for i in (0...count.length)
      puts "#{i}: #{count[i]}"
   end
}
